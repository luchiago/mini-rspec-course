require 'rails_helper'

RSpec.describe 'Enemies', type: :request do
  describe 'GET /enemies' do
    context 'when exists a enemy' do
      let!(:enemy) { create(:enemy) }

      before(:each) { get '/enemies/' }

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all the enemies' do
        expect(enemy).to have_attributes(json.first.except(
          'created_at',
          'updated_at'
        )
      )
      end
    end

    context 'when doesnot exists a enemy' do
      before(:each) { get '/enemies/' }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
      it 'returns a error message' do
        expect(response.body).to match(/No enemies/) 
      end
    end
  end

  describe 'POST /enemies' do
    context 'when send valid parameters' do
      let(:enemy_attributes) { FactoryBot.attributes_for(:enemy) }

      it 'returns created status' do
        post '/enemies/', params: { enemy: enemy_attributes }
        expect(response).to have_http_status(201)
      end

      it 'creates the enemy' do
        expect{
          post '/enemies/', params: { enemy: enemy_attributes }
        }.to change(Enemy, :count)
      end
    end

    context 'when send invalid parameters' do
      let(:enemy_attributes) { { name: '', level: '' } }

      it 'returns created status' do
        post '/enemies/', params: { enemy: enemy_attributes }
        expect(response).to have_http_status(400)
      end

      it 'creates the enemy' do
        expect{
          post '/enemies/', params: { enemy: enemy_attributes }
        }.to_not change(Enemy, :count)
      end
    end
  end

  describe 'GET /enemy' do
    context 'with a enemy created' do
      let(:enemy) { create(:enemy) }

      before(:each) { get "/enemies/#{enemy.id}" }

      it 'return status ok' do
        expect(response).to have_http_status(200)
      end
      it 'return the enemy' do
        expect(enemy).to have_attributes(json.except(
          'created_at',
          'updated_at'
        ))  
      end
    end

    context 'without a enemy created' do

      before(:each) { get '/enemies/0' }

      it 'return status not found' do
        expect(response).to have_http_status(404)
      end
      it 'return a error message' do
        expect(response.body).to match(/Couldn't find Enemy/)
      end
    end
  end

  describe 'PUT /enemies' do
    context 'when the enemy exists' do
      let(:enemy) { create(:enemy) }
      let(:enemy_attributes) { FactoryBot.attributes_for(:enemy) }

      before(:each) { put "/enemies/#{enemy.id}", params: { enemy: enemy_attributes } }

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
      it 'updates the record' do
        expect(enemy.reload).to have_attributes(enemy_attributes)
      end
      it 'returns the enemy updated' do
        expect(enemy.reload).to have_attributes(json.except(
            'created_at',
            'updated_at'
          )
        )
      end
    end

    context 'when the enemy does not exists' do
      before(:each) { put '/enemies/0', params: FactoryBot.attributes_for(:enemy) }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Enemy/)
      end
    end
  end

  describe 'DELETE /enemies' do
    context 'when the enemy exists' do
      let(:enemy) { create(:enemy) }

      before(:each) { delete "/enemies/#{enemy.id}" }

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
      it 'destroy the record' do
        expect { enemy.reload }.to raise_error ActiveRecord::RecordNotFound
      end
    end

    context 'when the enemy does not exist' do
      before(:each) { delete '/enemies/0' }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Enemy/)
      end
    end
  end
end
