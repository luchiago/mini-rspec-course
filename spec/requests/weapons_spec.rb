require 'rails_helper'

RSpec.describe 'Weapons', type: :request do
  describe 'GET /weapons' do

    let!(:weapons) { create_list(:weapon, 3) }

    it 'returns all weapons names' do
      get weapons_path
      weapons.each do |weapon|
        expect(response.body).to include(weapon.name)
      end
    end
    it 'returns all weapons current_power' do
      get weapons_path
      weapons.each do |weapon|
        expect(response.body).to include(weapon.current_power.to_s)
      end
    end
    it 'returns all weapons titles' do
      get weapons_path
      weapons.each do |weapon|
        expect(response.body).to include(weapon.title)
      end
    end

    context 'visit the page' do
      subject { page }
      before { visit weapons_path }
      it 'returns all weapons links' do
        weapons.each do |weapon|
          expect(page).to have_link('Details', href: weapon_path(weapon))
        end
      end
    end
  end

  describe 'POST /weapons' do
    context 'with valid parameters' do
      it 'the weapon is created' do
        weapon_attributes = FactoryBot.attributes_for(:weapon)
        post weapons_path, params: { weapon: weapon_attributes }
        expect(Weapon.last).to have_attributes(weapon_attributes)
      end
    end
    context 'with invalid parameters' do
      it 'the weapon is not created' do
        expect {
          invalid_attributes = { level: -1 }
          post weapons_path, params: { weapon: invalid_attributes }
        }.to_not change(Weapon, :count)
      end
    end
  end

  describe 'GET /destroy' do
    it 'querying the right id the weapon is deleted' do
      weapon = create(:weapon)
      expect {
        delete "/weapons/#{weapon.id}"
      }.to change(Weapon, :count)
    end
  end

  describe 'GET /show' do
    it 'all weapon details are shown' do
      weapon = create(:weapon)
      get weapon_path(weapon.id)
      expect(response.body).to include(weapon.name)
      expect(response.body).to include(weapon.description)
      expect(response.body).to include(weapon.power_base.to_s)
      expect(response.body).to include(weapon.power_step.to_s)
      expect(response.body).to include(weapon.level.to_s)
      expect(response.body).to include(weapon.title)
      expect(response.body).to include(weapon.current_power.to_s)
    end
  end
end
