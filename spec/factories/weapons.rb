FactoryBot.define do
  factory :weapon do
    name { FFaker::Lorem.word }
    description { FFaker::Lorem.paragraph }
    power_base { (3_000..10_000).to_a.sample }
    power_step { 100 }
    level { (1..99).to_a.sample }
  end
end
