Rails.application.routes.draw do
  resources :users, only: %i[index create]
  resources :weapons, only: %i[index create show destroy]
  resources :enemies, only: %i[index create show update destroy]
end
