class WeaponsController < ApplicationController

  def index
    @weapons = Weapon.all
  end

  def create
    @weapon = Weapon.create(weapon_params)
    redirect_to weapons_path
  end

  def destroy
    @weapon ||= find_weapon
    @weapon.destroy
  end

  def show
    @weapon ||= find_weapon
    @weapon
  end

  private

  def weapon_params
    params.require(:weapon).permit(:name, :description, :level, :power_base, :power_step)
  end

  def find_weapon
    @weapon = Weapon.find_by(id: params[:id])
  end
end
