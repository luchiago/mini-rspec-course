class EnemiesController < ApplicationController
  before_action :set_enemy, except: %i[index create]

  def index
    @enemies = Enemy.all
    if @enemies.blank?
      render json: { message: 'No enemies' }, status: :not_found
    else
      render json: @enemies, status: :ok
    end
  end

  def show
    render json: @enemy, status: :ok
  end

  def create
    @enemy = Enemy.new(enemy_params)
    if @enemy.save
      render json: @enemy, status: :created
    else
      render json: { errors: @enemy.errors }, status: :bad_request
    end
  end

  def update
    if @enemy.update(enemy_params)
      render json: @enemy, status: :ok
    else
      render json: { errors: @enemy.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @enemy.destroy
    head 204
  end

  private

  def enemy_params
    params.require(:enemy).permit(:name, :power_base, :power_step, :level, :kind)
  end

  def set_enemy
    @enemy = Enemy.find(params[:id])
  rescue ActiveRecord::RecordNotFound => e
    render json: { message: e.message }, status: :not_found
  end
end
